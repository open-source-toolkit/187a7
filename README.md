# MATLAB 可用编译器：MinGW-w64 6.3 (Win 64位系统)

## 简介

本仓库提供了一个适用于MATLAB和Simulink的编译器资源文件：**MinGW-w64 6.3**。该编译器支持将MATLAB脚本和Simulink模型转换为C程序，适用于Windows 64位系统。

## 资源来源

该资源文件来源于 [SourceForge](https://sourceforge.net/projects/mingw-w64/)。

## 用途

该编译器主要用于MATLAB和Simulink环境中，支持将MATLAB脚本和Simulink模型转换为C程序。通过使用该编译器，用户可以更方便地进行代码生成和模型部署。

## 安装步骤

详细的安装步骤可以参考以下链接：
[MinGW-w64 安装教程](https://jingyan.baidu.com/article/c843ea0b414cad37921e4a62.html)

## 关联文章

关于MATLAB编译器的更多信息，可以参考以下文章：
[MATLAB编译器使用指南](https://blog.csdn.net/qq_51559922/article/details/128791247)

## 查询编译器安装情况

在命令行中输入以下命令，可以查询当前系统中已安装的编译器：
```bash
mex -setup
```

## 更多语言支持

关于MATLAB支持的C/C++编译器选择，可以参考以下官方文档：
[选择C或C++编译器](https://ww2.mathworks.cn/help/matlab/matlab_external/choose-c-or-c-compilers.html)

## MATLAB支持的编译器列表

MATLAB支持的编译器详细列表可以参考以下链接：
[MATLAB支持的编译器](https://ww2.mathworks.cn/support/requirements/supported-compilers.html)

## 注意事项

- 请确保您的系统为Windows 64位系统。
- 安装过程中请按照教程步骤进行操作，确保编译器正确安装。
- 安装完成后，建议使用`mex -setup`命令检查编译器是否正确配置。

## 贡献

如果您在使用过程中遇到任何问题或有改进建议，欢迎提交Issue或Pull Request。

## 许可证

本仓库中的资源文件遵循其原始许可证。请在使用前仔细阅读相关许可证信息。

---

希望本资源能够帮助您顺利完成MATLAB和Simulink的开发工作！